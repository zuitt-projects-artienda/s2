CLI Commands
ls - list files and folders contained by the current directory.

pwd - present working directory - shows the current folder we are working on.

cd <folderName/path to folder>- change directoris - change the current folder/directory we are currently working on with our CLI (terminal/gitbash)

mkdir <folderName> - create a new directory/folder

touch - used to create files

Sublime Text 3
	-a lightweight text/file editor. Sublime Text is much more lightweight than VSCode or Visual Studio. It uses less RAM/Memory which is important because we also, as devs, have our google chrome open.

Basic Git Commands:

When using git for the first time in computer:

Configure our Git:

git config --global user.email "<emailFromGitLab/Github>"
	-This will allow us to identify the account from Gitlab/Github who will push/upload files in our online Gitlab/Github servces.

git config --global user.name "<nameFromGitLab/Github>"
	-This will allow us to identify tge name of the user who is trying to upload/push files in our online Github/Gitlab. This also allow us to determine the name of the person who uploaded the latest commit/version in our repo.

What is SSH Key?

	SSH or Secure Shell Key are tools used to authenticate the uploading or of other tasks when manipulating or using git repo. It allowas us to push/upload into our online git repo without the use of passwords. (Direct connection between computer and online git)

What is a local/online repo

	A local repo is a folder that is tracked with git. When we use git init in a folder, git will able to track the updates of the folder files. Then we can push those changes as a version /commit to our online repo.

	An online repo is a folder /project in a online git service like Github or GitLab. This is a folder also tracked with git but its primary purpose is to add files and folders from a local repo.

Pushing Commands:

Pushing for the first time:

git init - allows us to initialize a local folder as a local git repo. This means that the folder and its files is now tracked by git. You can then push the update tracked in the folder into your online repo.

git add . - allows us to add all files and update into a new commit of our files and folders  to be uploaded to our online repo.

git commit -m "<commitMessage>"  - This will allow us to create a new version of our files and folders based on the update added using git add.
We are also able to add a proper and appropriate title or message that defines the changes or update made to files and folders.
	-commit message usually start with a verb. (add discussion, update index html)

git remote add origin <gitURLofOnlineReport> - this wil allow us to connect an online report to local repo.  "origin" is the default or conventional name for a online repo. Local repo can connect to muultiple online repos, by convention , the first online repo connected is called "origin".

git push origin master - allows us to push the latest commit created into our online repo that is designated as "origin". master is the branch to push our commit to. master branch is the default version or main version of our files in the repo.

Pushing Flow:

When pushing for the first time:
git init -> git add . -> git commit -m"<commit message>" -> git remote add origin <paste url online repo> -> git push origin master

Pushing update:
git add . -> git commit -m"<commit message>" -> git push origin master