My name is Kimberly Carmel K. Artienda. You can call me Kim. I am a Zuitt Coding Bootcamper!

Below are my top 5 hobbies:
1. I like watching movies, series and crime documentaries.
2. I love to travel and roadtrips.
3. I love pigging out with friends and try new restaurants around metro.
4. I love to play badminton.
5. I love to cook.